package com.ananasproject.hundredwants;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.widget.Toast;
import com.ananasproject.hundredwants.additionalpreferences.TimePreference;
import static com.ananasproject.hundredwants.R.xml.preference;


public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(preference);
            final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

            // finding Views
            SwitchPreference notificationSwitch = (SwitchPreference) findPreference("notification_key");
            final TimePreference timePreference = (TimePreference) findPreference("notificationTime_key");
            timePreference.setEnabled(notificationSwitch.isChecked());
            if (timePreference.isEnabled()) {
                String savedTime = sharedPref.getString("notificationTime_key", "");
                timePreference.setSummary(getString(R.string.notification) + " " + savedTime);
            }

            // adding event to view to enable/disable the timePickerPreference depending on the state of the switch
            notificationSwitch.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    if (!((SwitchPreference) preference).isChecked()) {
                        timePreference.setEnabled(true);
                    } else {
                        timePreference.setEnabled(false);
                        sharedFunctions.clearDailyNotification(getActivity());
                        timePreference.setSummary(getString(R.string.settings_missingnotificationtime));
                    }
                    return true;
                }
            });

            timePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    timePreference.setSummary(getString(R.string.notification) + " " + ((String) o));

                    String[] time = ((String) o).split(":");
                    int hour = Integer.parseInt(time[0]);
                    int min = Integer.parseInt(time[1]);

                    sharedFunctions.setDailyNotification(getActivity(), hour, min);
                    return true;
                }
            });

            Preference.OnPreferenceChangeListener opcl = new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    Toast.makeText(getActivity(), getActivity().getString(R.string.settings_restartyourapp), Toast.LENGTH_LONG).show();
                    return true;
                }
            };

            SwitchPreference dateActiveSwitch = (SwitchPreference) findPreference("dateActive_key");
            SwitchPreference dateInactiveSwitch = (SwitchPreference) findPreference("dateInactive_key");

            dateActiveSwitch.setOnPreferenceChangeListener(opcl);
            dateInactiveSwitch.setOnPreferenceChangeListener(opcl);

            Preference deleteAll = (Preference) findPreference("deleteall_key");
            deleteAll.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                        sharedFunctions.showDeleteConfirmationDialog(getActivity(), "", -1, -1, true);
                    return false;
                }
            });
        }

    }
}
