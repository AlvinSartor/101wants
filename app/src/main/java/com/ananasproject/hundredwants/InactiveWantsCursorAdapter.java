package com.ananasproject.hundredwants;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ananasproject.hundredwants.data.WantsContract;
import static com.ananasproject.hundredwants.sharedFunctions.moveWantToActive;


public class InactiveWantsCursorAdapter extends CursorAdapter {

    public InactiveWantsCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 /* flags */);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        // Find individual views that we want to modify in the list item layout
        TextView wantTextView = (TextView) view.findViewById(R.id.wantText);
        TextView numberTextView = (TextView) view.findViewById(R.id.number);
        numberTextView.setVisibility(View.GONE);
        wantTextView.setPaintFlags(wantTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        TextView dateTextView = (TextView) view.findViewById(R.id.wantDate);
        Log.d("INACTIVE CHECK", "want: " + wantTextView.getText() + " :: size: " + wantTextView.getTextSize());

        // Find the columns of pet attributes that we're interested in
        int textColumnIndex = cursor.getColumnIndex(WantsContract.InactiveEntry.COLUMN_TEXT);
        int numberColumnIndex = cursor.getColumnIndex(WantsContract.InactiveEntry.COLUMN_NR);
        int IDcolumnIndex = cursor.getColumnIndex(WantsContract.InactiveEntry._ID);
        int dateColumnIndex = cursor.getColumnIndex(WantsContract.InactiveEntry.COLUMN_INSERT_DATE);
        int date2ColumnIndex = cursor.getColumnIndex(WantsContract.InactiveEntry.COLUMN_COMPLETION_DATE);

        // Read the pet attributes from the Cursor for the current want
        final String text = cursor.getString(textColumnIndex);
        final int ID = cursor.getInt(IDcolumnIndex);
        final String date = cursor.getString(dateColumnIndex);
        final String date2 = cursor.getString(date2ColumnIndex);

        // Update the TextViews with the attributes for the current want
        wantTextView.setText(text);
        //numberTextView.setText("#" + nr);
        int nrOfDays = sharedFunctions.daysInBetween(date, date2);
        String days = (nrOfDays == 1 ? context.getString(R.string.day) : context.getString(R.string.days));
        dateTextView.setText(date + " - " + date2 + "   ->   " + nrOfDays  + " " + days);

        if (!sharedPref.getBoolean("dateInactive_key", true))
            dateTextView.setVisibility(View.GONE);

        Button btUndo = (Button) view.findViewById(R.id.btDone);
        btUndo.setBackgroundResource(R.drawable.ic_undo_black_24dp);
        btUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toastText = "\"" + text + "\" " + context.getString(R.string.signed_as_undone);
                Snackbar.make(view, toastText, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                moveWantToActive(context, ID, text, date);
            }
        });

        (view.findViewById(R.id.btDelete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getBoolean("safedelete_key", true))
                    sharedFunctions.showDeleteConfirmationDialog(context, text, ID, -1, false);
                else
                    context.getContentResolver().delete(
                            ContentUris.withAppendedId(WantsContract.InactiveEntry.CONTENT_URI, ID),
                            null,
                            null);
            }
        });

    }




}
