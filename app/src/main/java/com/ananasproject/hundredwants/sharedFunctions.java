package com.ananasproject.hundredwants;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import com.ananasproject.hundredwants.data.WantsContract;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ALARM_SERVICE;
import static com.ananasproject.hundredwants.ActiveWantsActivityFragment.nextNumber;


public class sharedFunctions {

    /** Given two dates, returns the number of days in between */
    public static int daysInBetween(String d1, String d2) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);

            long diff = date2.getTime() - date1.getTime();
            return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    /** Insert an active want into the DB */
    public static void insertActiveWant(Context context, int nr, String text) {
        ContentValues values = new ContentValues();
        values.put(WantsContract.ActiveEntry.COLUMN_NR, nr);
        values.put(WantsContract.ActiveEntry.COLUMN_TEXT, text);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        values.put(WantsContract.ActiveEntry.COLUMN_INSERT_DATE, date);

        // Insert a new row into the provider using the ContentResolver.
        // Receive the new content URI that will allow us to access the want's data in the future.
        Uri newUri = context.getContentResolver().insert(WantsContract.ActiveEntry.CONTENT_URI, values);
    }

    /** Moves a Want from the active DB to the inactive DB */
    public static void moveWantToInactive(Context context, int ID, String text, int nr, String date) {
        context.getContentResolver().delete(
                ContentUris.withAppendedId(WantsContract.ActiveEntry.CONTENT_URI, ID),
                null,
                null);

        ContentValues values = new ContentValues();
        values.put(WantsContract.InactiveEntry.COLUMN_NR, nr);
        values.put(WantsContract.InactiveEntry.COLUMN_TEXT, text);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String completingDate = sdf.format(new Date());
        values.put(WantsContract.InactiveEntry.COLUMN_INSERT_DATE, date);
        values.put(WantsContract.InactiveEntry.COLUMN_COMPLETION_DATE, completingDate);

        // Insert a new row into the provider using the ContentResolver.
        // Receive the new content URI that will allow us to access the want's data in the future.
        Uri newUri = context.getContentResolver().insert(WantsContract.InactiveEntry.CONTENT_URI, values);
        decreaseNumberOfWants(context, nr, 1000);
        ActiveWantsActivityFragment.nextNumber.setText("#" + getNextWantNumber(context));
    }

    /** Moves a Want from the inactive DB to the active DB */
    public static void moveWantToActive(Context context, int ID, String text, String date) {
        context.getContentResolver().delete(
                ContentUris.withAppendedId(WantsContract.InactiveEntry.CONTENT_URI, ID),
                null,
                null);

        ContentValues values = new ContentValues();
        values.put(WantsContract.InactiveEntry.COLUMN_NR, getNextWantNumber(context));
        values.put(WantsContract.InactiveEntry.COLUMN_TEXT, text);
        values.put(WantsContract.InactiveEntry.COLUMN_INSERT_DATE, date);

        // Insert a new row into the provider using the ContentResolver.
        // Receive the new content URI that will allow us to access the want's data in the future.
        Uri newUri = context.getContentResolver().insert(WantsContract.ActiveEntry.CONTENT_URI, values);
        ActiveWantsActivityFragment.nextNumber.setText("#" + getNextWantNumber(context));
    }

    /** Searches in the DB to find the next number available */
    public static int getNextWantNumber(Context context) {
        String[] projections = new String[] {WantsContract.ActiveEntry.COLUMN_NR};
        Cursor cursor = context.getContentResolver().query(WantsContract.ActiveEntry.CONTENT_URI, projections, null, null, null);
        int count = cursor.getCount();
        if (count == 0) return 1;
        int[] allNumbers = new int[count];

        while(cursor.moveToNext())
            allNumbers[cursor.getPosition()] = cursor.getInt(0);

        cursor.close();

        int maxNr = 0;
        for (int nr : allNumbers) if (maxNr < nr) maxNr = nr;

        return maxNr + 1;
    }

    /** Used to increase the attribute 'number' of the wants in the DB, starting from a certain point */
    public static void increaseNumberOfWants(Context context, int startingFrom, int endingAt) {
        // select all wants
        int max = getNextWantNumber(context) - 1;
        Cursor cursor = context.getContentResolver().query(WantsContract.ActiveEntry.CONTENT_URI,
                new String[] {WantsContract.ActiveEntry.COLUMN_NR, WantsContract.ActiveEntry._ID},
                null,
                null,
                null);

        // foreach want we check if (# > startingFrom) and we update it adding +1
        while (cursor.moveToNext()) {
            int oldNr = cursor.getInt(cursor.getColumnIndex(WantsContract.ActiveEntry.COLUMN_NR));
            if (oldNr >= startingFrom && oldNr < endingAt) {
                ContentValues values = new ContentValues();
                values.put(WantsContract.ActiveEntry.COLUMN_NR, oldNr + 1);
                Uri wantWeAreGonnaChange = Uri.withAppendedPath(WantsContract.ActiveEntry.CONTENT_URI, "" + cursor.getInt(cursor.getColumnIndex(WantsContract.ActiveEntry._ID)));
                context.getContentResolver().update(wantWeAreGonnaChange, values, null, null);
            }
        }

        cursor.close();
    }

    /** Used to decrease the attribute 'number' of the wants in the DB, starting from a certain point */
    public static void decreaseNumberOfWants(Context context, int startingFrom, int endingAt) {
        // select all wants
        int max = getNextWantNumber(context) - 1;
        Cursor cursor = context.getContentResolver().query(WantsContract.ActiveEntry.CONTENT_URI,
                new String[] {WantsContract.ActiveEntry.COLUMN_NR, WantsContract.ActiveEntry._ID},
                null,
                null,
                null);

        // foreach want we check if (# > startingFrom) and we update it adding +1
        while (cursor.moveToNext()) {
            int oldNr = cursor.getInt(cursor.getColumnIndex(WantsContract.ActiveEntry.COLUMN_NR));
            if (oldNr >= startingFrom && oldNr < endingAt) {
                ContentValues values = new ContentValues();
                values.put(WantsContract.ActiveEntry.COLUMN_NR, oldNr - 1);
                Uri wantWeAreGonnaChange = Uri.withAppendedPath(WantsContract.ActiveEntry.CONTENT_URI, "" + cursor.getInt(cursor.getColumnIndex(WantsContract.ActiveEntry._ID)));
                context.getContentResolver().update(wantWeAreGonnaChange, values, null, null);
            }
        }

        cursor.close();
    }

    public static void deleteAll(Context context) {
        context.getContentResolver().delete(WantsContract.ActiveEntry.CONTENT_URI, null, null);
        context.getContentResolver().delete(WantsContract.InactiveEntry.CONTENT_URI, null, null);
        ActiveWantsActivityFragment.nextNumber.setText("#" + getNextWantNumber(context));
    }

    /** Show a deleteConfirmationDialog and then delete the record
     *
     * -> set wantNr to -1 if you don't want to update the number of active records
     *
     * */
    public static void showDeleteConfirmationDialog(final Context context, String want, final int wantID,
                                              final int wantNr, final boolean isTotal) {
        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the postivie and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String message = (isTotal ? context.getString(R.string.totalCancellationConfirmal)
                                    : context.getString(R.string.cancellationConfirmal) + " \"" + want + "\"?");

        builder.setMessage(message);
        builder.setPositiveButton(context.getString(R.string.deleteButton), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (isTotal)
                    deleteAll(context);
                else {
                    Uri u = (wantNr == -1 ?
                            //inactive
                        ContentUris.withAppendedId(WantsContract.InactiveEntry.CONTENT_URI, wantID) :
                            //active
                        ContentUris.withAppendedId(WantsContract.ActiveEntry.CONTENT_URI, wantID));

                    context.getContentResolver().delete(u, null, null);

                    if (wantNr != -1) {
                        decreaseNumberOfWants(context, wantNr, 1000);
                        nextNumber.setText("#" + getNextWantNumber(context));
                    }
                }
            }
        });
        builder.setNegativeButton(context.getString(R.string.cancelButton), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog
                // and continue editing the pet.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void modifyWantNumber(Context context, int wantID, int newNr) {
        ContentValues values = new ContentValues();
        values.put(WantsContract.ActiveEntry.COLUMN_NR, newNr);
        context.getContentResolver().update(
                Uri.withAppendedPath(WantsContract.ActiveEntry.CONTENT_URI, "" + wantID),
                values, null, null);
    }

    public static void createNotification(Context context, String title, String text) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_ananas)
                        .setContentTitle(title)
                        .setContentText(text);

        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setColor(0x00796B);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(001, mBuilder.build());
    }

    static AlarmManager alarmManager = null;
    static Intent receiverIntent = null;
    public static void setDailyNotification(Context context, int h, int m) {
        clearDailyNotification(context);
        alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
        receiverIntent = new Intent(context , Receiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, receiverIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Set the alarm to start at the specified hour
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, h);
        calendar.set(Calendar.MINUTE, m);
        calendar.set(Calendar.SECOND, 0);

        //set repetition
        String alarmTime = "" + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE);
        Toast.makeText(context, "Setting alarm at: " + alarmTime, Toast.LENGTH_LONG).show();
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 60 * 24, pendingIntent);
    }


    public static void clearDailyNotification(Context context) {
        if (alarmManager != null && receiverIntent != null) {
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, receiverIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            alarmManager = null;
        }
    }
}
