package com.ananasproject.hundredwants.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Alvin on 27/12/2016.
 */

public class WantsContract {

    private WantsContract() {}

    public static final String CONTENT_AUTHORITY = "com.ananasproject.hundredwants";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_ACTIVE = "activeWants";
    public static final String PATH_CATEGORIES = "categories";
    public static final String PATH_INACTIVE = "inactiveWants";


    public static final class ActiveEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_ACTIVE);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ACTIVE;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_ACTIVE;

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_NR = "number";
        public final static String COLUMN_TEXT = "text";
        public final static String COLUMN_CATEGORY = "category";
        public final static String COLUMN_INSERT_DATE = "insertDate";

    }

    public static final class InactiveEntry implements BaseColumns {

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_INACTIVE);
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INACTIVE;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_INACTIVE;

        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_NR = "number";
        public final static String COLUMN_TEXT = "text";
        public final static String COLUMN_CATEGORY = "category";
        public final static String COLUMN_INSERT_DATE = "insertDate";
        public final static String COLUMN_COMPLETION_DATE = "completionDate";

    }


}
