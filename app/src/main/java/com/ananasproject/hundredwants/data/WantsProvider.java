package com.ananasproject.hundredwants.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import com.ananasproject.hundredwants.data.WantsContract.*;

import static com.ananasproject.hundredwants.data.WantsContract.PATH_ACTIVE;
import static com.ananasproject.hundredwants.data.WantsContract.PATH_INACTIVE;

/**
 * Created by Alvin on 27/12/2016.
 */

public class WantsProvider extends ContentProvider {

    private static final int ACTIVE_WANTS = 100;
    private static final int ACTIVE_WANT_ID = 101;
    private static final int INACTIVE_WANTS = 102;
    private static final int INACTIVE_WANT_ID = 103;

    /**
     * UriMatcher object to match a content URI to a corresponding code.
     * The input passed into the constructor represents the code to return for the root URI.
     * It's common to use NO_MATCH as the input for this case.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        // Static initializer. This is run the first time anything is called from this class.
        sUriMatcher.addURI(WantsContract.CONTENT_AUTHORITY, PATH_ACTIVE, ACTIVE_WANTS);
        sUriMatcher.addURI(WantsContract.CONTENT_AUTHORITY, PATH_ACTIVE + "/#", ACTIVE_WANT_ID);
        sUriMatcher.addURI(WantsContract.CONTENT_AUTHORITY, PATH_INACTIVE, INACTIVE_WANTS);
        sUriMatcher.addURI(WantsContract.CONTENT_AUTHORITY, PATH_INACTIVE + "/#", INACTIVE_WANT_ID);
    }

    /** Database helper object */
    private WantsDatabaseHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new WantsDatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // Get readable database
        SQLiteDatabase database = mDbHelper.getReadableDatabase();
        // This cursor will hold the result of the query
        Cursor cursor;

        // Figure out if the URI matcher can match the URI to a specific code
        int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTIVE_WANTS:
                cursor = database.query(PATH_ACTIVE, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case ACTIVE_WANT_ID:
                selection = ActiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                cursor = database.query(PATH_ACTIVE, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INACTIVE_WANTS:
                cursor = database.query(PATH_INACTIVE, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case INACTIVE_WANT_ID:
                selection = InactiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                cursor = database.query(PATH_INACTIVE, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query unknown URI " + uri);
        }

        // Set notification URI on the Cursor,
        // so we know what content URI the Cursor was created for.
        // If the data at this URI changes, then we know we need to update the Cursor.
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        // Return the cursor
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTIVE_WANTS:
                return insertWant(uri, contentValues, true);
            case INACTIVE_WANTS:
                return insertWant(uri, contentValues, false);
            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);
        }
    }


    private Uri insertWant(Uri uri, ContentValues values, boolean isActive) {

        int nr = values.getAsInteger(ActiveEntry.COLUMN_NR);
        String text = values.getAsString(ActiveEntry.COLUMN_TEXT);

        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        String tableName = (isActive ? PATH_ACTIVE : PATH_INACTIVE);

        // Insert the new pet with the given values
        long id = database.insert(tableName, null, values);
        // If the ID is -1, then the insertion failed. Log an error and return null.
        if (id == -1) { Log.e("PROVIDER", "Failed to insert row for " + uri); return null; }

        // Notify all listeners that the data has changed for the pet content URI
        getContext().getContentResolver().notifyChange(uri, null);

        // Return the new URI with the ID (of the newly inserted row) appended at the end
        return ContentUris.withAppendedId(uri, id);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection,
                      String[] selectionArgs) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTIVE_WANT_ID:
                selection = ActiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateWant(uri, contentValues, selection, selectionArgs, true);
            case INACTIVE_WANT_ID:
                selection = ActiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                return updateWant(uri, contentValues, selection, selectionArgs, false);
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
    }

    private int updateWant(Uri uri, ContentValues values, String selection, String[] selectionArgs, boolean isActive) {

        // If there are no values to update, then don't try to update the database
        if (values.size() == 0)
            return 0;

        // Otherwise, get writeable database to update the data
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Perform the update on the database and get the number of rows affected
        String tableName = (isActive ? PATH_ACTIVE : PATH_INACTIVE);
        int rowsUpdated = database.update(tableName, values, selection, selectionArgs);

        // If 1 or more rows were updated, then notify all listeners that the data at the
        // given URI has changed
        if (rowsUpdated != 0)
            getContext().getContentResolver().notifyChange(uri, null);

        // Return the number of rows updated
        return rowsUpdated;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Get writeable database
        SQLiteDatabase database = mDbHelper.getWritableDatabase();

        // Track the number of rows that were deleted
        int rowsDeleted;

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTIVE_WANTS:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(PATH_ACTIVE, selection, selectionArgs);
                break;
            case ACTIVE_WANT_ID:
                // Delete a single row given by the ID in the URI
                selection = ActiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                rowsDeleted = database.delete(PATH_ACTIVE, selection, selectionArgs);
                break;
            case INACTIVE_WANTS:
                // Delete all rows that match the selection and selection args
                rowsDeleted = database.delete(PATH_INACTIVE, selection, selectionArgs);
                break;
            case INACTIVE_WANT_ID:
                // Delete a single row given by the ID in the URI
                selection = ActiveEntry._ID + "=?";
                selectionArgs = new String[] { String.valueOf(ContentUris.parseId(uri)) };
                rowsDeleted = database.delete(PATH_INACTIVE, selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Deletion is not supported for " + uri);
        }

        // If 1 or more rows were deleted, then notify all listeners that the data at the
        // given URI has changed
        if (rowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // Return the number of rows deleted
        return rowsDeleted;
    }

    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ACTIVE_WANTS:
                return ActiveEntry.CONTENT_LIST_TYPE;
            case ACTIVE_WANT_ID:
                return ActiveEntry.CONTENT_ITEM_TYPE;
            case INACTIVE_WANTS:
                return InactiveEntry.CONTENT_LIST_TYPE;
            case INACTIVE_WANT_ID:
                return InactiveEntry.CONTENT_ITEM_TYPE;

            default:
                throw new IllegalStateException("Unknown URI " + uri + " with match " + match);
        }
    }


}
