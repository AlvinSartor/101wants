package com.ananasproject.hundredwants.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.ananasproject.hundredwants.data.WantsContract.*;

import static com.ananasproject.hundredwants.data.WantsContract.PATH_ACTIVE;
import static com.ananasproject.hundredwants.data.WantsContract.PATH_INACTIVE;

/**
 * Created by Alvin on 27/12/2016.
 */

public class WantsDatabaseHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "shelter.db";
    private static final int DATABASE_VERSION = 1;

    public WantsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create a String that contains the SQL statement to create the pets table
        String SQL_CREATE_ACTIVE_TABLE =  "CREATE TABLE IF NOT EXISTS " + PATH_ACTIVE + " ("
                + ActiveEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ActiveEntry.COLUMN_NR + " INTEGER NOT NULL, "
                + ActiveEntry.COLUMN_TEXT + " TEXT NOT NULL, "
                + ActiveEntry.COLUMN_CATEGORY + " INTEGER NOT NULL DEFAULT 0, "
                + ActiveEntry.COLUMN_INSERT_DATE + " DATE NOT NULL);";

        // Execute the SQL statement
        sqLiteDatabase.execSQL(SQL_CREATE_ACTIVE_TABLE);

        // Create a String that contains the SQL statement to create the pets table
        String SQL_CREATE_INACTIVE_TABLE =  "CREATE TABLE IF NOT EXISTS " + PATH_INACTIVE + " ("
                + InactiveEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + InactiveEntry.COLUMN_NR + " INTEGER NOT NULL, "
                + InactiveEntry.COLUMN_TEXT + " TEXT NOT NULL, "
                + InactiveEntry.COLUMN_CATEGORY + " INTEGER NOT NULL DEFAULT 0, "
                + InactiveEntry.COLUMN_INSERT_DATE + " DATE NOT NULL,"
                + InactiveEntry.COLUMN_COMPLETION_DATE + " DATE NOT NULL);";

        // Execute the SQL statement
        sqLiteDatabase.execSQL(SQL_CREATE_INACTIVE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // The database is still at version 1, so there's nothing to do be done here.
    }
}
