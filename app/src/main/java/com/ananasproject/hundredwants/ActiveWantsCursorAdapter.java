package com.ananasproject.hundredwants;

import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ananasproject.hundredwants.data.WantsContract;
import com.ananasproject.hundredwants.dragndroplist.*;

import static com.ananasproject.hundredwants.ActiveWantsActivityFragment.nextNumber;
import static com.ananasproject.hundredwants.sharedFunctions.*;



public class ActiveWantsCursorAdapter extends CursorAdapter implements DragNDropAdapter {
    ActiveWantsActivityFragment boss;
    int mHandler;

    public ActiveWantsCursorAdapter(Context context, Cursor c, ActiveWantsActivityFragment a, int handlerID) {
        super(context, c, 0 /* flags */);
        mHandler = handlerID;
        boss = a;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

        // Find individual views that we want to modify in the list item layout
        TextView wantTextView = (TextView) view.findViewById(R.id.wantText);
        final TextView numberTextView = (TextView) view.findViewById(R.id.number);
        TextView dateTextView = (TextView) view.findViewById(R.id.wantDate);

        // Find the columns of pet attributes that we're interested in
        int textColumnIndex = cursor.getColumnIndex(WantsContract.ActiveEntry.COLUMN_TEXT);
        int numberColumnIndex = cursor.getColumnIndex(WantsContract.ActiveEntry.COLUMN_NR);
        int IDcolumnIndex = cursor.getColumnIndex(WantsContract.ActiveEntry._ID);
        int dateColumnIndex = cursor.getColumnIndex(WantsContract.ActiveEntry.COLUMN_INSERT_DATE);

        // Read the pet attributes from the Cursor for the current want
        final String text = cursor.getString(textColumnIndex);
        final int nr = cursor.getInt(numberColumnIndex);
        final int ID = cursor.getInt(IDcolumnIndex);
        final String date = cursor.getString(dateColumnIndex);

        // Update the TextViews with the attributes for the current want
        wantTextView.setText(text);
        numberTextView.setText("#" + nr);
        dateTextView.setText(date);
        if (!sharedPref.getBoolean("dateActive_key", true))
            dateTextView.setVisibility(View.GONE);

        (view.findViewById(R.id.btDone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String toastText = "\"" + text + "\" " + context.getString(R.string.signed_as_done);
                Snackbar.make(view, toastText, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                moveWantToInactive(context, ID, text, nr, date);
            }
        });

        (view.findViewById(R.id.btDelete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sharedPref.getBoolean("safedelete_key", true))
                    sharedFunctions.showDeleteConfirmationDialog(context, text, ID, nr, false);
                else {
                    context.getContentResolver().delete(
                            ContentUris.withAppendedId(WantsContract.ActiveEntry.CONTENT_URI, ID),
                            null,
                            null);
                    decreaseNumberOfWants(context, nr, 1000);
                    nextNumber.setText("#" + getNextWantNumber(context));
                }
            }
        });

    }




    @Override
    public Cursor swapCursor(Cursor newCursor) {
        Cursor c = super.swapCursor(newCursor);
        return c;
    }

    @Override
    public int getDragHandler() { return mHandler; }

    @Override
    public void onItemDrag(DragNDropListView parent, View view, int position, long id) {  }

    @Override
    public void onItemDrop(DragNDropListView parent, View view, int startPosition, int endPosition, long id) {
//        int position = mPosition[startPosition];

        if (startPosition > endPosition) {
            increaseNumberOfWants(view.getContext(), endPosition + 1, startPosition + 1);
            //Log.d("ONITEMDROP", "start: " + startPosition + ". End: " + endPosition);
        } else if (startPosition < endPosition) {
            decreaseNumberOfWants(view.getContext(), startPosition + 1, endPosition + 2);
            //Log.d("ONITEMDROP", "start: " + startPosition + ". End: " + endPosition);
        }

        modifyWantNumber(view.getContext(), (int) id, endPosition + 1);
        boss.reload();

        //swapCursor(reloadData(view.getContext()));
    }
}
