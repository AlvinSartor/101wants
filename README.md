# README #   

The '101 wants' is an ancient buddhist technique that is still used in today's society, based on the idea that you can't obtain what you want if you don't know what you want. It is derived from the Law Of Attraction and other theories such as New Thought.
   
There are people that believe in magic. However, 101 wants can also be explained in terms of logic: when we have a clear desire, we tend to become more attentive and seek new opportunities that fulfil our desires.
   
However, in order for any of these desires to become true, you need to be proactive, waiting in the sofa is useless.         
  
  
The app is available at:         
https://play.google.com/store/apps/details?id=com.ananasproject.hundredwants     


The application is pretty simple: it consists in an interface to a database where all the wants are stored.

The wants can be created and deleted, not modified, to keep them as fixed as possible. This makes even easier the handling of the operations that have to be performed on the database.